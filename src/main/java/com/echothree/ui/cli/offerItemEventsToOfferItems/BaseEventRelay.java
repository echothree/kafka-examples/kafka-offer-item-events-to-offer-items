// --------------------------------------------------------------------------------
// Copyright 2002-2022 Echo Three, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// --------------------------------------------------------------------------------

package com.echothree.ui.cli.offerItemEventsToOfferItems;

import com.echothree.ui.cli.offerItemEventsToOfferItems.schema.Event;
import com.echothree.ui.cli.offerItemEventsToOfferItems.schema.EventAdapter;
import com.google.common.base.Charsets;
import com.google.common.net.MediaType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.ToNumberPolicy;
import com.google.gson.reflect.TypeToken;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.http.HttpHeaders;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseEventRelay {

    private static final Logger LOG = LoggerFactory.getLogger(BaseEventRelay.class);

    protected static final Gson GSON = new GsonBuilder()
            .serializeNulls()
            .setObjectToNumberStrategy(ToNumberPolicy.LONG_OR_DOUBLE)
            .registerTypeAdapter(Event.class, new EventAdapter())
            .create();

    protected final Headers HEADERS_JSON = new RecordHeaders()
            .add(new RecordHeader(HttpHeaders.CONTENT_TYPE, MediaType.JSON_UTF_8.toString().getBytes(Charsets.UTF_8)));

    protected final String bootstrapServers;
    protected final String incomingTopic;
    protected final String outgoingTopic;

    public BaseEventRelay(final String bootstrapServers, final String incomingTopic, final String outgoingTopic) {
        this.bootstrapServers = bootstrapServers;
        this.incomingTopic = incomingTopic;
        this.outgoingTopic = outgoingTopic;
    }

    public Map<String, Object> toMap(String json) {
        var typeToken = new TypeToken<Map<String, Object>>() {
        };

        Map<String, Object> map = GSON.fromJson(json, typeToken.getType());

        return map == null ? Collections.emptyMap() : map;
    }

    public String toJson(Map<String, Object> map)  {
        return GSON.toJson(map);
    }

    public boolean getBoolean(Object bean, String name)
            throws Exception {
        return (Boolean)PropertyUtils.getProperty(bean, name);
    }

    public String getString(Object bean, String name)
            throws Exception {
        return (String)PropertyUtils.getProperty(bean, name);
    }

    protected Map<String, Object> getMap(Object bean, String name)
            throws Exception {
        return (Map<String, Object>)PropertyUtils.getProperty(bean, name);
    }

    public void eventLoop(final String groupId)
            throws Exception {
        // Consumer Configuration
        var consumerProperties = new Properties();
        consumerProperties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        consumerProperties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        consumerProperties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        consumerProperties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        consumerProperties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        consumerProperties.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, Boolean.FALSE.toString());

        // Producer Configuration
        var producerProperties = new Properties();
        producerProperties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        producerProperties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        producerProperties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // Create Consumer
        try(var consumer = new KafkaConsumer<String, String>(consumerProperties)) {
            consumer.subscribe(List.of(incomingTopic));

            // Create Producer
            try(var producer = new KafkaProducer<String, String>(producerProperties)) {
                while(true) {
                    var records = consumer.poll(Duration.ofMillis(100));

                    for (var partition : records.partitions()) {
                        var partitionRecords = records.records(partition);
                        for (var record : partitionRecords) {
                            processRecord(record, producer);
                        }

                        // "at-least-once" delivery guarantee
                        var lastOffset = partitionRecords.get(partitionRecords.size() - 1).offset();
                        consumer.commitSync(Collections.singletonMap(partition, new OffsetAndMetadata(lastOffset + 1)));
                    }
                }
            }
        }
    }

    public void processRecord(ConsumerRecord<String, String> record, Producer<String, String> producer)
            throws Exception {
        var value = record.value();
        var valueMap = toMap(value);
        var eventMap = getMap(valueMap, "event");
        var event = GSON.fromJson(toJson(eventMap), Event.class);

        LOG.info("value: " + value);
        LOG.info("eventTypeName: " + event.eventTypeName() + ", id: " + event.id());

        if(!"DELETE".equals(event.eventTypeName())) {
            var offerItemMap = getMap(valueMap, "offerItem");
            if(offerItemMap == null) {
                LOG.info("offerItemMap == null");
            } else {
                var id = getString(offerItemMap, "id");
                var offerItemJson = toJson(offerItemMap);

                LOG.info("offerItemJson: " + offerItemJson);

                send(producer, event, id, offerItemJson);
            }
        } else {
            LOG.info("ignoring DELETE event");
        }
    }

    protected void send(final Producer<String, String> producer, final Event event, final String id, final String valueJson)
            throws InterruptedException, ExecutionException {
        var future = producer.send(new ProducerRecord<>(outgoingTopic, null,
                event.eventTime(), id, valueJson, HEADERS_JSON));

        future.get();
        producer.flush();
    }

}
